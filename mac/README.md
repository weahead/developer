# We ahead Developer 101 - Mac OSX/macOS

This document aims to provide the documentation necessary to help developers
working at We ahead AB on Mac OSX/macOS to get up and running with their
environments and to better understand the common vision, goals and standards we
adhere to.

Please read the [general documentation](../README.md) first.


### Software

The recommended software for managing programs and packages is
[Homebrew](http://brew.sh).

Please visit the website and follow the installation instructions.

If you already have it installed make sure you are up to date by running:

```
brew update
brew upgrade
```


### Development environment

The development environment consists of [Dinghy](https://github.com/codekitchen/dinghy).
It uses Docker and some other technologies to provide a smooth developer
experience on macOS. You can read more about Dinghy on [their project page](https://github.com/codekitchen/dinghy)

We have written a command line tool to help ease setup and development workflow.
We have named it `wa`. Read on for how to install and use the tool.


## Getting started

The following commands will install We aheads CLI tool so that your laptop has
everything it needs to begin developing and creating awesome stuff in our
repositories (and elsewhere too!).


### Install Homebrew

- Go to [Homebrew homepage](http://brew.sh/) and follow the installation
  instructions.

- Once installtion finish test the installtion:
  ```
  brew doctor
  ```
  Warnings can *usually* be ignored, errors are a cause for concern though.


### Installing We aheads Homebrew Tap

- Run:
  ```
  brew tap weahead/tools
  ```


### Installing We aheads CLI

- Run:
  ```
  brew install wa
  ```

- Test the installation:
  ```
  wa version
  ```


### Setting up your development environment

- Run:
  ```
  wa setup dev
  ```

  And follow the on screen instructions carefully.


### Taking it for a spin

- Run:
  ```
  wa status
  ```

  If all is good, then success! You now have a running Docker installation
  backed with all the services needed to aid you in development.


### Start development

The instructions above is a general setup to have the same basic environment for
every project, but to start development in a specific project it might require
some more setup. Detailed instructions will be provided in the project that you
are going to work with.
