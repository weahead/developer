# We ahead Developer 101

This repository aims to provide the documentation necessary to help developers
working at We ahead AB to get up and running with their environments and to
better understand the common vision, goals and standards we adhere to.

It currently only contains installation instructions for Mac OSX/macOS due
to that being our main platform. Instructions for Windows and Linux will be
written when a developer that uses that platform is available.


## Environment

Our preferred environment is based on container technology delivered by Docker.
All new projects have at least development environments created in Docker. Some
also have staging and/or production environments available, it depends on the
project and client.

Having opted for Docker technology it means that Dockers dictates the support
each platform receives. Most of our developers work on MacBooks Pro with
Mac OSX/macOS. A few work with PC and Linux or Windows.


### Note on Docker for Mac/Windows

We have had some issues that prevented us from using it as our default
environment. Most notably smart DNS, local proxy and speedy filesystem solution
that Dinghy provides.


## Installation instructions

- [Mac OSX/macOS](mac/README.md)
- Windows (TBD)
- Linux (TBD)


## License

[X11](LICENSE)

